 # Build the Physics Module library

scheme_root_dir = src/hardlibrary
# Build settings

CXX = g++-11
CXXFLAGS = -fPIC -std=c++14 -DDOUBLE_RESULTS  -DNDEBUG -DHAVE_HDF5 -DWITH_CHECKPOINT $(CANTERACXXFLAGS) -Ofast -march=native  -finline-limit=20000 -mcmodel=medium

LIBS = $(CANTERALIBS)

CPPFLAGS = -Isrc/api $(addprefix -I, $(wildcard $(scheme_root_dir)/*))  -I$(HDFROOT)/include $(addprefix -I, $(wildcard $(scheme_root_dir)/*/PhysicalModules))

LDFLAGS = -shared -rdynamic

sources := $(wildcard $(scheme_root_dir)/*/*.cpp) $(wildcard $(scheme_root_dir)/*/*/*.cpp)
headers := $(wildcard $(scheme_root_dir)/*/*.hpp) $(wildcard $(scheme_root_dir)/*/*/*.hpp) $(wildcard $(scheme_root_dir)/../physics/*.hpp)
objects = $(sources:.cpp=.o)
scheme_lib = libPM.so

# Make rules

$(scheme_lib): $(objects)
	$(CXX) $(LDFLAGS) -o $@ $(objects) $(LIBS)

$(objects): $(headers)

.PHONY : clean
clean:
	rm -rf $(objects) *.so $(scheme_root_dir)/**/*.o $(scheme_root_dir)/*/*/*.o
