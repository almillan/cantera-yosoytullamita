![](cantera-logo.png)

# Cantera installation requirements 

Depending on the OS the requirements could be diferents 

## Windows 
This is an Scons [config](Windows/cantera.conf) file example. 

The requirememts are:

* Python
> Download it from the oficial site [download](https://www.python.org/downloads/) or install it from the [.7z](Windows/python-3.10.11-amd64.7z) file. 
>
> Finish it by adding Python folder to enviroment variables (e.g. *C:\Program Files\Python310  & C:\Program Files\Python310\Scripts*)
* Scons 
> Just type **pip install scons** on a windows terminal
* Python libraries
> pip install numpy 
>
> pip install cython (safe version 0.29.33)
> 
> pip install setuptools
>
> Note: if *distutils.command.bdist_msi* is missing then uncrompress [setuptools](Windows/setuptools-60.2.0.7z)
 and do:
>
> pip uninstall setuptools
>
> python setup install
 
* C++ compilers 
> Use the tdm-gcc compiler from [website](https://jmeubank.github.io/tdm-gcc/download/) or from [zip](Windows/tdm64-gcc-10.3.0-2.7z) file.
>
> Add it location to path (e.g. *C:\TDM-GCC-64\bin*)
* Boost 
> Just extract [boost file](Windows/boost_1_82_0.7z) or download from [boost](https://www.boost.org/users/download/) site and place it in your default directory (e.g. *C:\Program Files\boost*)
* Fortran Compilers. 
> Todo: I couldn't find a proper compiler/config option to work. 

Note: some times it need to compile a few times. 
## Ubuntu

This is an Scons [config](Ubuntu/cantera.conf) file example. 

The requirememts are:

