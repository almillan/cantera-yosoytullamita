"""
A freely-propagating, premixed hydrogen flat flame with multicomponent
transport properties.

Requires: cantera >= 2.5.0
Keywords: combustion, 1D flow, premixed flame, multicomponent transport,
          saving output
"""

import cantera as ct
import numpy as np
import math
import multiprocessing as mp

# Simulation parameters
p         = 1.0e5  # pressure [Pa]
T       = 300.0  # unburned gas temperature [K]
gamma     = 1.4
# ~ T       = 300*(p*1e-5)**((gamma-1)/gamma)
phi       = 1.0
width     = 0.002  # m
loglevel  = 0 # amount of diagnostic output (0 to 8)

gas = ct.Solution('CH4_AE1S.cti')
file_name = 'Sl_CH4_1bar_300K_ae1s.csv'
# ~ gas = ct.Solution('sandiego20161214.cti','SanDiego_mix')
# ~ file_name = 'Sl_CH4_1bar_300K_sd.csv'

transport = 'AVBP'  # UnityLewis AVBP Mix Multi 

fuel     = {'CH4': 1.0, 'H2': 0.0} # Fuel composition
oxidizer = {'O2': 1, 'N2': 3.7619} # Oxygen composition
# ~ oxidizer = {'O2': 0.120, 'N2': 1.0} # Oxygen composition for 20bar

# limits of integration
# 1bar: 0.2-14 
# 20bar: 0.4-8
# 20barGamma: 0.1-12   // 0.3-20
phi_v = np.linspace(0.4,2.0,40) 
# ~ phi_v = np.logspace(math.log10(0.3),math.log10(3),20) 
t_v = np.logspace(math.log10(300),math.log10(800),40) 
p_v = np.logspace(math.log10(1e5),math.log10(40e5),10) 
S_l = []
var = phi_v

# flame integrator
def compute_SL(phi):
	gas.TP = T, p
	gas.set_equivalence_ratio(phi, fuel, oxidizer)
	f = ct.FreeFlame(gas, width=width)
	# Tolerance properties
	tol_ss = [1.0e-5, 1.0e-9]  # [rtol atol] for steady-state problem
	tol_ts = [1.0e-5, 1.0e-9]  # [rtol atol] for time stepping
	f.flame.set_steady_tolerances(default=tol_ss)
	f.flame.set_transient_tolerances(default=tol_ts)
	f.transport_model = transport
	# Set time steps whenever Newton convergence fails
	# ~ f.set_time_step(1.0e-5, [2, 5, 10, 100, 1000])  # s
	# ~ f.set_time_step_factor(1.2)
	# Max number of times the Jacobian will be used before it must be re-evaluated
	# ~ f.set_max_jac_age(10, 10)
	# ~ f.energy_enabled = False
	# ~ f.set_refine_criteria(ratio=10.0, slope=1, curve=1)
	# ~ f.solve(loglevel=loglevel, refine_grid=True, auto=True)
	f.set_refine_criteria(ratio=2.5, slope=0.03, curve=0.02)
	f.solve(loglevel=loglevel, refine_grid=True, auto=True)
	
	print('{0:7f} '.format(phi)+' {0:7f} '.format(f.velocity[0]))
	# ~ f.write_csv('adiabatic_flame_1sfb.csv', species='Y', quiet=False)
	return f.velocity[0]

print('---------------------------------------------------------------')
print('phi [-]   S_L [m/s]')
print('---------------------------------------------------------------')
# Parallel loop
pool_obj = mp.Pool()
S_l      = pool_obj.map(compute_SL,var)
# ~ # Serial loop
# ~ for phi in phi_v:
	# ~ S_l.append(compute_SL(var))
# ~ # for test	
# ~ S_l.append(compute_SL(var))

phicurve = np.zeros((len(var),2))
phicurve[:,0] = var
phicurve[:,1] = S_l
np.savetxt(file_name, phicurve,delimiter='\t',header="phi \tSL", fmt="%2.4f", comments='')

# write the velocity, temperature, density, and mole fractions to a CSV file
# ~ f.write_csv('adiabatic_flame_1svc_TH.csv', species='Y', quiet=False)
