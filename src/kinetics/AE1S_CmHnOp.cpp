/**
 *  @file AE1S_CmHnOp.cpp Homogeneous kinetics in ideal gases
 */

// This file is part of Cantera. See License.txt in the top-level directory or
// at https://cantera.org/license.txt for license and copyright information.

#include "cantera/kinetics/AE1S_CmHnOp.h"
#include "cantera/base/Parser.h"

using namespace std;

namespace Cantera
{

AE1S_CmHnOp::AE1S_CmHnOp(thermo_t* thermo) :
     GasKinetics(thermo),
     NSP{7} // hard coded check way of make it automatic
{
     printf("-----------------------------------\n");
     printf("WARNING: Using customized kinetics.\n");
     printf("-----------------------------------\n");
     // ae1s_fuel = "nada";
     //
     // ifstream ae1s_mechanismdb("ae1s_database.dat");
     // if (ae1s_mechanismdb.good()){
     //   // read_mixture("ae1s_database.dat");
     // } else {
     //   cout<<"FATAL ERROR: cannot find required input files (ae1s_database.dat) for simplified chemical model !"<<endl;
 	   //   exit(-1);
     // }
     //
     // std::cout << ae1s_fuel << std::endl;

}

void AE1S_CmHnOp::getNetProductionRates(doublereal* net){
    ae1s_fuel = "nada";
    ifstream ae1s_mechanismdb("ae1s_database.dat");
    if (ae1s_mechanismdb.good()){
      read_mixture("ae1s_database.dat");

    } else {
      cout<<"FATAL ERROR: cannot find required input files (ae1s_database.dat) for simplified chemical model !"<<endl;
      exit(-1);
    }
    //------------------------
    // Basic fuel definitions
    //------------------------
    doublereal nN = ae1s_nN,               // fuel coefficients CH4 = C_nH_mO_p
               nM = ae1s_nM,
               nP = ae1s_nP;
    doublereal beta = ae1s_beta;           // Recombination ratio of CO:H2
    //-----------------------
    // Single-step mechanism
    //-----------------------              // W = C_f^nu/2 C_o^nu/2 A exp(E/RT)
    doublereal nuVL = ae1s_nuVL,           // Order of reaction
               AVL  = ae1s_AVL,            // Arrhenius preexponential factor
               TVL  = ae1s_TVL;            // Arrhenius activation temperature
    // Phi correction                      // F_i = 1-DrVL*(1-tanh(-4/nu_o (phi-phiVL)/DphiVL))
    doublereal DrVL1    = ae1s_DrVL1,      // Velocity factor
               DphiVL1  = ae1s_DphiVL1,    // phi range
               phiVL1   = ae1s_phiVL1,     // phi location
               DrVL2    = ae1s_DrVL2,      // Velocity factor
               DphiVL2  = ae1s_DphiVL2,    // phi range
               phiVL2   = ae1s_phiVL2;     // phi location
    // //------------------------
    // // Basic fuel definitions
    // //------------------------
    // doublereal nN = 1.0,                   // fuel coefficients CH4 = C_nH_mO_p
    //           nM = 4.0,
    //           nP = 0.0;
    // doublereal beta = 0.32;                // Recombination ratio of CO:H2
    // //-----------------------
    // // Single-step mechanism
    // //-----------------------              // W = C_f^nu/2 C_o^nu/2 A exp(E/RT)
    // doublereal nuVL = 1.12,                // Order of reaction
    //           AVL  = 1.04E8,              // Arrhenius preexponential factor
    //           TVL  = 2.20E4 / 1.987207;   // Arrhenius activation temperature
    // // Phi correction                      // F_i = 1-DrVL*(1-tanh(-4/nu_o (phi-phiVL)/DphiVL))
    // doublereal DrVL1    = 0.17,            // Velocity factor
    //           DphiVL1  = 1.31e-1,         // phi range
    //           phiVL1   = 0.72,            // phi location
    //           DrVL2    = 0.46,            // Velocity factor
    //           DphiVL2  = 2.90e-1,         // phi range
    //           phiVL2   = 1.24;            // phi location
    //==========================================================================
    // Initialization
    //==========================================================================
    bool           useVC = true; // use variable stoichiometric coefficients
    bool           useAn = false; // Use analitical solution or fitted
    int              NSP = thermo().nSpecies();
    int ich4{0}, io2{1}, ico2{2}, ih2o{3}, ico{4}, ih2{5}, in2{6};
    // Variable initialization
    doublereal R_g  = 8.31446261815324 ;      // Universal gas constant  [J / K mol]
    doublereal Conc[NSP]={};                  // Molar concentration
    doublereal Conc_M {0.0};                  // Total molar concentration
    doublereal      WR   = 0.0;    // Reaction rates constants and global rate
    doublereal Coef[NSP] = {};     // Stoichiometric reaction coefficients
    doublereal      P    = thermo().pressure();    // Current pressure
    doublereal      T    = thermo().temperature(); // Current temperature
    doublereal      TH   = max(min(T,3000.0),300.0); // Current temperature
    doublereal      rho  = thermo().density();     // Current density
    doublereal      H0   = thermo().enthalpy_mass();          // Current enthalpy
    doublereal     ALOGT = log(TH);
    doublereal     RTm1  = 1.0e0/(TH*R_g);
    const doublereal*  Y = thermo().massFractions(); // Current mass fraction
    vector_fp Wk = thermo().molecularWeights(); // Molecular Weights
    for (int k = 0; k < NSP; k++) {Wk[k] *=1.0e-3;}
    for (int k = 0; k < NSP; k++) {
      Conc[k] = rho * max(Y[k],1.0e-15) / Wk[k]*1e-6 ;
      Conc_M += Conc[k];
    }
    //==========================================================================
    // Variable stoichiometric coefficient A. Millan-Merino(2023)
    //==========================================================================
    // Stiochiometric coefficients for elementary reactions.
    int        NEQ          = 3;
    doublereal nu[NEQ][NSP] = {};
    doublereal Keq[NEQ]     = {1.0,1.0,1.0}, // Equilibrium constant
               dKeq_dT[NEQ] = {1.0,1.0,1.0}; // Equilibrium constant
     // filling the first reaction:  CnHmOp + (n+m/4-p/2)O2 -> nCO2 + m/2 H2O
     nu[0][ich4] =-1.0, nu[0][io2]  =-(nN+nM*0.25-nP*0.5);
     nu[0][ico2] =  nN, nu[0][ih2o] =  nM*0.5 ;
     // filling the second reaction: CO2              -> CO  + 1/2 O2
     nu[1][ico2] = -1.0,nu[1][ico]  =  1.0, nu[1][io2]  =  0.5;
     // filling the third reaction:  H2O              -> H2  + 1/2 O2
     nu[2][ih2o] = -1.0,nu[2][ih2]  =  1.0, nu[2][io2]  =  0.5;
    // Current equivalence ratio and mole numbers
    doublereal N0 {0.0},  N_0[NSP] = {};
    // normalized with the present oxygen concentration
    for (int k = 0; k < NSP; k++) {
            N_0[k]  = Conc[k]/Conc[io2]*(-nu[0][io2]) ;
            N0      += N_0[k];
    }
    doublereal Phi_min {0.01}, Phi_max {3.00};                      // Limits for equilibrium computations
    doublereal Phi     = max(min(N_0[ich4],Phi_max),Phi_min) ;      // This is the current moles
    doublereal n_ox {N_0[ico]+N_0[ih2]} ;
    // Asymptotic limit
    doublereal xi_ox_0 = (nN*2.0+nM*0.5-nP) * (Phi-1.0) ;   // Asimptotic limit (MINIMUM)
    doublereal xi_2_0  = beta*max(xi_ox_0,0.0),  xi_3_0    = max(xi_ox_0,0.0)-xi_2_0;
    doublereal xi_0[NEQ] = {}, xi_i[NEQ] = {}, dxi_i[NEQ] = {};
               xi_0[0]   = Phi;     xi_0[1]   = xi_2_0;  xi_0[2]   = xi_3_0;
               xi_i[0]   = xi_0[0]; xi_i[1]   = xi_0[1]; xi_i[2]   = xi_0[2];
    // Computing equilibrium solution
    doublereal Nad = 0.0, N_ad[NSP] = {};
    for (int k = 0; k < NSP; k++) {
         N_ad[k]  = N_0[k];
      for (int kk = 0; kk < NEQ; kk++) {
         N_ad[k] += nu[kk][k]*xi_0[kk];
      }
         Nad      = Nad + N_ad[k] ;
    }
    // --------------------------------------------
    // Thermodynamic Condition
    // --------------------------------------------
    setState_HPX(H0,P,N_ad);               // Set current gas to adiabatic state
    doublereal T_asp = thermo().temperature(); // TH; //
    doublereal T_min = 1000.0;             // Minimum temperature to apply the method
    doublereal Tad   = min(T_asp, 3000.0); // Adiabatic temperature limited
    doublereal Delta_H[NSP] = {};          // Reaction enthalpies (constant)
    doublereal hrt[NSP];
    thermo().getEnthalpy_RT(hrt);
    for (int k = 0; k < NEQ; k++) {
        for (int kk = 0; kk < NSP; kk++) {
          Delta_H[k] += hrt[kk]*nu[k][kk];
        }
          Delta_H[k] *= R_g*T_asp;
    }
    doublereal Cp_asp = thermo().cp_mole() * 1.0e-3, N_asp = Nad;
    doublereal F_0 = T_asp, F_0p = T_asp, J_0 = 0.0;
    // thermo().setState_TPY(T,P,Y);          // Return current gas to original state
    // ----------------------------------------------------------------
    // Iterative loop of equilibrium solution
    // ----------------------------------------------------------------
    doublereal k_ox{0.0}, xi_ox {0.0}, xi_2{0.0}, xi_3{0.0}, Theta{0.0}, R_0 {0.0};
    doublereal dk_ox{0.0}, dxi_ox{0.0}, dTheta {0.0};
    doublereal dxi_2_dxi_ox{0.0}, dxi_3_dxi_ox{0.0}, dxi_2{0.0}, dxi_3{0.0};
    doublereal az{0.0}, bz{0.0}, Theta_{0.0}, pz{0.0};
    doublereal daz{0.0}, dbz{0.0}, dpz{0.0};
    doublereal K_r{0.0}, a1{0.0}, a2{0.0}, a3{0.0}, a4{0.0}, A_w{0.0}, B_w{0.0};
    // Numerical parameters SSM: Yosoytullamita. Thesis A. Millan-Merino 2020
    int   i{0}, IMAX{10};
    doublereal kappa{0.99}, DeltaT_0{0.2}, DeltaT_1{0.2}, DeltaT{T_asp},
               armijo{0.0}, fac{1.01}, tol{1e-3};
    while (DeltaT_1 >= tol && ++i <=IMAX ) {
    // while (DeltaT_1 >= tol && ++i <=IMAX ) {
        // Evaluation of thermodynamic constants
      for (int k = 1; k < NEQ; k++) {
        Keq[k]     = funcEq(nu[k], P, Tad) ;
        dKeq_dT[k] = H_RTTEq(nu[k], P, Tad) ;
      }
        k_ox       = ( (N_0[ico2]+nN*Phi )*Keq[1]     + (N_0[ih2o]+0.5*nM*Phi)*Keq[2]     ) * sqrt(2.0*N0+(nM*0.5-2.0+nP)*Phi) ;
        dk_ox      = ( (N_0[ico2]+nN*Phi )*dKeq_dT[1] + (N_0[ih2o]+0.5*nM*Phi)*dKeq_dT[2] ) * sqrt(2.0*N0+(nM*0.5-2.0+nP)*Phi) ;
        //-------------------------------------------------------------
        // solution of: K_ox = (n_ox + xi_ox) ( -xi_ox_0 + xi_ox )^0.5
        //-------------------------------------------------------------
        R_0    = 27.0*pow(k_ox,2) + 4.0*pow(xi_ox_0+n_ox,3) ;
      if ( R_0 >= 0.0) { // REAL CASE
        Theta  = pow(     k_ox*sqrt(27*max(R_0, 1e-16)) + 27.0*pow(k_ox,2) + 2.0*pow(xi_ox_0+n_ox,3),1.0/3.0)/pow(2.0,1.0/3.0) ;
        dTheta = pow( 3.0*k_ox*sqrt( 3*max(R_0, 1e-16)) +  9.0*    k_ox    + 2.0*pow(xi_ox_0+n_ox,3),1.0/3.0)/pow(max(R_0,1e-15),2) *2.0 / sqrt(3.0)/pow(2.0,1.0/3.0) ; // new
        xi_ox  = ( Theta                      + pow(xi_ox_0+n_ox,2)/Theta                      + (xi_ox_0 - 2.0*n_ox) )/3.0 ; // stop here
        dxi_ox = ( 1.0                        - pow(xi_ox_0+n_ox,2)/pow(Theta,2)                                      )/3.0*dTheta ; // new
      } else           { // COMPLEX CASE
        az     = k_ox*sqrt( 27.0*max(-R_0, 1e-16) );
        daz    = ( 2.0*pow(n_ox + xi_ox_0,3) + 27.0*pow(k_ox,2) ) /sqrt(max(-R_0,1e-15)) *(-6.0)*sqrt(3.0);
        bz     = 27.0*pow(k_ox,2) + 2.0*pow(xi_ox_0+n_ox,3) ;
        dbz    = 54*k_ox;
        Theta_ = pow( pow(az,2) + pow(bz,2), 1.0/6.0)/(pow(2.0,1.0/3.0)) ;
        pz     = atan2( az , (bz+1e-16) ) / 3.0 ; //atan( bz / (az+1e-16) ) / Pi ;
        dpz    =  ( bz*daz - az*dbz )/( pow(az,2) + pow(bz,2) )/3.0 ;
        xi_ox  = (Theta_*cos(pz) + pow(xi_ox_0+n_ox,2.0)/Theta_*cos(pz) + xi_ox_0 - 2.0*n_ox ) /3.0 ;
        dxi_ox = (Theta_         + pow(xi_ox_0+n_ox,2.0)/Theta_)*(-sin(pz))*dpz /3.0 ;
      }
        xi_ox  = min( xi_ox, 3.0*Phi ); // physical limits
        xi_ox  = max( xi_ox, 0.0 );     // physical limits
        //-------------------------------------------------------------
        // solution of: K_r = (a1-xi_3)/(a2+xi_3) (a3-xi_3)/(a4+xi_3)
        //-------------------------------------------------------------
        K_r    = Keq[1] / Keq[2] ;
        a1     = N_0[ico] + xi_ox ;    a2     = N_0[ih2] ;
        a3     = N_0[ih2o]+nM*0.5*Phi; a4     = N_0[ico2]+nN*Phi - xi_ox ;
        A_w    = a1*a3-a2*a4*K_r ;     B_w    = a1+a3+(a2+a4)*K_r ;
        // recovering the original progress variables
        xi_3         = (sqrt( max(pow(B_w, 2.0)+4.0*A_w*(K_r-1.0), 1e-16))-B_w)/(2.0*(K_r-1.0)) ;
        xi_3         = min(xi_3,2.0*Phi); // physical limits
        xi_3         = max(xi_3,0.0);     // physical limit
        xi_2         = xi_ox - xi_3 ;
        dxi_3_dxi_ox = ( K_r*( N_0[ih2] + xi_3                              ) + N_0[ih2o]+nM*0.5*Phi - xi_3                      )
                     / ( K_r*( N_0[ico2]+N_0[ih2]+nN*Phi - xi_ox + 2.0*xi_3 ) + N_0[ih2o]+N_0[ico]+nM*0.5*Phi + xi_ox - 2.0*xi_3 );
        dxi_3_dxi_ox = min(max(dxi_3_dxi_ox, 1.0e-6),1.0-1.0e-6) ;
        dxi_2_dxi_ox = 1.0 - dxi_3_dxi_ox;
        dxi_2        = dxi_2_dxi_ox * dxi_ox * dk_ox ;
        dxi_3        = dxi_3_dxi_ox * dxi_ox * dk_ox ;
        xi_i[1] = xi_2; xi_i[2] = xi_3;
        dxi_i[1] = dxi_2; dxi_i[2] = dxi_3;
        //  SHAMANSKII METHOD: (Modified Newton-Raphson)
        // Function equilibrium objective and its Jacobian
        F_0      = T_asp - Tad;
        J_0      =        -1.0;
      for (int k = 0; k < NEQ; k++) {
        F_0     -= Delta_H[k]/Cp_asp * ( xi_i[k] - xi_0[k])/N_asp;
        J_0     -= Delta_H[k]/Cp_asp * (dxi_i[k]          )/N_asp;
      }
        // Jacobian equilibrium objective
        DeltaT   = - F_0 / J_0;
        // Safety condition: delta T too high
        DeltaT   = DeltaT/max(abs(DeltaT),1.0e-15) * min(abs(DeltaT),T_asp*0.1);
        // Armijo rule
        DeltaT_0 = DeltaT_1;
        DeltaT_1 = abs(DeltaT/T_asp);
        armijo   = (1.0-1.0e-4*kappa)*DeltaT_0;
      if (armijo > DeltaT_1) {
        kappa   = min(kappa*1.5,0.99);
      } else {
        kappa   = max(kappa*0.5,0.1);
      }
        // Adiabatic temperature update
        Tad     += kappa*DeltaT;
        Tad      = max(min(Tad, 4500.0),200.0);
    }
    thermo().setState_TPY(T,P,Y);          // Return current gas to original state
    // // safety condition: avoid random values
    // if (DeltaT_1 >= tol * 20.0 ) {
    //   for (int k = 0; k < NEQ; k++) {
    //     xi_i[k]  = xi_0[k];
    //   }
    // }
    //---------------------------------------------
    // Stiochiometric coefficients computations
    //---------------------------------------------
    for (int k = 0; k < NSP; k++) {
        N_ad[k]  = N_0[k];
      for (int kk = 0; kk < NEQ; kk++) {
        N_ad[k] += nu[kk][k]*xi_i[kk];
      }
        Coef[k]  = ( N_ad[k] - N_0 [k] );
    }
    // // Constant values
    //     Coef[ich4]  = -1.0; //-1.0 ;
    //     Coef[io2]   = -2.0; //-(nN+nM*0.25-nP*0.5) ;
    //     Coef[ico2]  = 1.0; // nN ;
    //     Coef[ih2o]  = 2.0; //nM*0.5 ;
    //     Coef[ico]   = 0.0; //nM*0.5 ;
    //     Coef[ih2]   = 0.0; //nM*0.5 ;
    //==========================================================================
    // REACTION RATE
    // WARNING: Only valid for flame propagation
    //==========================================================================
    // Global equation A. Millan-Merino (2023)
    // Equivalence ratio correction function
    doublereal F_f = 0.5,    // Fuel order ratio
               nuf = F_f * nuVL, nuO = (1.0 - F_f) * nuVL;
    doublereal FF = 1.0; // Laminar flame phi correction
    FF  = 1.0 - DrVL1 * (1.0 - tanh(-4.0 / 2.0 * (Phi - phiVL1) / DphiVL1));
    FF *= 1.0 - DrVL2 * (1.0 - tanh(-4.0 / 2.0 * (Phi - phiVL2) / DphiVL2));
    // Reaction rate constant
    doublereal kf = AVL * exp(-TVL / TH);
    WR = 1.0e3 * kf * pow(Conc[ich4], nuf) * pow(Conc[io2], nuO) * FF;
    // WR = pow(Conc[ich4],0.56)*pow(Conc[io2],0.56)*2.3722e+08*exp(-22000.0/1.987/TH)*1.0e3; // first solution
    //==========================================================================
    // Species reaction rate
    //==========================================================================
    for (int k = 0; k < NSP; k++) {
      net[k] = WR * Coef[k];
    }
} // AE1S_CmHnOp::getNetProductionRates

doublereal AE1S_CmHnOp::funcEq(const doublereal* NU, doublereal P, doublereal T ) {
  thermo().setState_TP(T,P);
  int NSP = thermo().nSpecies();
  doublereal grt[NSP];
  thermo().getGibbs_RT_ref(grt);
  doublereal nuSum{0.0}, gsum{0.0};
  const doublereal patm = 1.0e5; //101325.; //[Pa] 1 atm

  for (int isp = 0; isp < NSP; ++isp) {
    nuSum += NU[isp];
    gsum  += grt[isp] * NU[isp];
  }
  return exp(- gsum) * pow(patm / P, nuSum);
}
doublereal AE1S_CmHnOp::H_RTTEq(const doublereal* NU, doublereal P, doublereal T ) {
  thermo().setState_TP(T,P);
  int NSP = thermo().nSpecies();
  doublereal hrt[NSP];
  thermo().getEnthalpy_RT(hrt);
  doublereal hsum{0.0};

  for (int isp = 0; isp < NSP; ++isp) {
    hsum  += hrt[isp] * NU[isp];
  }
  return hsum/T;
}
void AE1S_CmHnOp::setState_HPX(doublereal H0, doublereal P, const doublereal* X) {
  thermo().setMoleFractions(X);
  thermo().setState_HP(H0,P);
}
doublereal AE1S_CmHnOp::Positive(doublereal X ){
  // doublereal a = 9.9455e-02, //
  //            b = 0.33026,    //
  //            c = 10.0;       //
  doublereal f = 200; //
  doublereal x0 = 1.0/f;    //
  doublereal c = (1+log(f))*x0;       //
  if(X<x0) {
    return exp((X-c)*f);
  } else{
    return X;
  }
  // doublereal f=10.0;
  // return max( tanh((X-1/f)*f)/f+1/f,X);
}
/**
 *  Parse the specific mixture_param.dat input file of
 *  AVBP v7 ++. Use the Parser.cpp located in ../base
 */
void AE1S_CmHnOp::read_mixture(std::string inputfile){
    Parser parser;
    Param* param;

    parser.parseFile(inputfile);
    // Get number of mixtures in database
    size_t n_mixtures = parser.nbParamOccurence("mixture_name");
    size_t idx_beg = std::string::npos;
    size_t idx_end = std::string::npos;

    // Loop through all occurences of keyword "mixture name"
    for (size_t i=0; i< n_mixtures; ++i) {
      param = parser.getParam("mixture_name",i);
      std::string current_str = param->readString(0);
      // Check if mixture_name is the one requested in the cti file
      if ( current_str == m_thermo[0]->name() ) {
        // Store bounding idx to get important info
      	idx_beg = parser.getParamNumber("mixture_name",i);
      	idx_end = parser.getParamNumber("mixture_name",i+1);
      }
    }
    // Get an error if requested mixture does not match any entry in database
    if (idx_beg == idx_end) {
    	        cout<<"FATAL ERROR: cannot find the requested mechanism in the 'a_database.dat' file.\n \
    	        Make sure that the name of your gas instance in the Cantera mechanism (.cti/.xml) file matches that provided in the 'mixture_database.dat'."<<endl;
    	        exit(-1);
    }
    // // Otherwise read important data. NB if keyword does not exist, error is managed in Parser.cpp
    // //------------------------
    // // Basic fuel definitions
    // //------------------------
    // fuel coefficients CH4 = C_nH_mO_p
    param = parser.getParam("nN", idx_beg, idx_end);
    ae1s_nN = param->readDouble(0);
    param = parser.getParam("nM", idx_beg, idx_end);
    ae1s_nM = param->readDouble(0);
    param = parser.getParam("nP", idx_beg, idx_end);
    ae1s_nP = param->readDouble(0);
    // Recombination ratio of CO:H2
    param = parser.getParam("beta", idx_beg, idx_end);
    ae1s_beta = param->readDouble(0);
    // //-----------------------
    // // Single-step mechanism
    // //-----------------------              // W = C_f^nu/2 C_o^nu/2 A exp(E/RT)
    // Order of reaction
    param = parser.getParam("nu", idx_beg, idx_end);
    ae1s_nuVL = param->readDouble(0);
    // Arrhenius preexponential factor
    param = parser.getParam("A", idx_beg, idx_end);
    ae1s_AVL = param->readDouble(0);
    // Arrhenius activation temperature
    param = parser.getParam("T", idx_beg, idx_end);
    ae1s_TVL = param->readDouble(0);
    // // Phi correction                      // F_i = 1-DrVL*(1-tanh(-4/nu_o (phi-phiVL)/DphiVL))
    // Velocity factor
    param = parser.getParam("DrVL1", idx_beg, idx_end);
    ae1s_DrVL1 = param->readDouble(0);
    // phi range
    param = parser.getParam("DphiVL1", idx_beg, idx_end);
    ae1s_DphiVL1 = param->readDouble(0);
    // phi location
    param = parser.getParam("phiVL1", idx_beg, idx_end);
    ae1s_phiVL1 = param->readDouble(0);
    // Velocity factor
    param = parser.getParam("DrVL2", idx_beg, idx_end);
    ae1s_DrVL2 = param->readDouble(0);
    // phi range
    param = parser.getParam("DphiVL2", idx_beg, idx_end);
    ae1s_DphiVL2 = param->readDouble(0);
    // phi location
    param = parser.getParam("phiVL2", idx_beg, idx_end);
    ae1s_phiVL2 = param->readDouble(0);

}
} // namespace Cantera
