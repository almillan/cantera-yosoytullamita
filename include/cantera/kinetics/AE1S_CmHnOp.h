/**
 * @file AE1S_CmHnOp.h
 * @ingroup chemkinetics
 */

// This file is part of Cantera. See License.txt in the top-level directory or
// at https://cantera.org/license.txt for license and copyright information.

#include "GasKinetics.h"

namespace Cantera
{

/**
 * Kinetics manager for elementary gas-phase chemistry. This kinetics manager
 * implements standard mass-action reaction rate expressions for low-density
 * gases.
 * @ingroup kinetics
 */
class AE1S_CmHnOp : public GasKinetics
{
public:
    //! @name Constructors and General Information
    //! @{

    //! Constructor.
    /*!
     *  @param thermo  Pointer to the gas ThermoPhase (optional)
     */
    AE1S_CmHnOp(thermo_t* thermo = 0);

    virtual std::string kineticsType() const {
        return "Single";
    }

    //! @}
    //! @name Reaction Rates Of Progress
    //! @{

    virtual void getNetProductionRates(doublereal* net);
    virtual void read_mixture(std::string s);

protected:
    // std::vector<thermo_t*> thermo_aux;



    // //! Reaction index of each falloff reaction
    // std::vector<size_t> m_fallindx;
    //
    // //! Map of reaction index to falloff reaction index (i.e indices in
    // //! #m_falloff_low_rates and #m_falloff_high_rates)
    // std::map<size_t, size_t> m_rfallindx;
    //
    // //! Rate expressions for falloff reactions at the low-pressure limit
    // Rate1<Arrhenius> m_falloff_low_rates;
    //
    // //! Rate expressions for falloff reactions at the high-pressure limit
    // Rate1<Arrhenius> m_falloff_high_rates;
    //
    // FalloffMgr m_falloffn;
    //
    // ThirdBodyCalc m_3b_concm;
    // ThirdBodyCalc m_falloff_concm;
    //
    // Rate1<Plog> m_plog_rates;
    // Rate1<ChebyshevRate> m_cheb_rates;
    //
    // //! @name Reaction rate data
    // //!@{
    // doublereal m_logp_ref;
    // doublereal m_logc_ref;
    // doublereal m_logStandConc;
    // vector_fp m_rfn_low;
    // vector_fp m_rfn_high;
    //
    // doublereal m_pres; //!< Last pressure at which rates were evaluated
    // vector_fp falloff_work;
    // vector_fp concm_3b_values;
    // vector_fp concm_falloff_values;
    // //!@}
    //
    // void processFalloffReactions();
    //
    // void addThreeBodyReaction(ThreeBodyReaction& r);
    // void addFalloffReaction(FalloffReaction& r);
    // void addPlogReaction(PlogReaction& r);
    // void addChebyshevReaction(ChebyshevReaction& r);
    //
    // void modifyThreeBodyReaction(size_t i, ThreeBodyReaction& r);
    // void modifyFalloffReaction(size_t i, FalloffReaction& r);
    // void modifyPlogReaction(size_t i, PlogReaction& r);
    // void modifyChebyshevReaction(size_t i, ChebyshevReaction& r);
    //
    // //! Update the equilibrium constants in molar units.
    // void updateKc();
private:
    int NSP;
    std::string ae1s_fuel;
    doublereal ae1s_nN,ae1s_nM,ae1s_nP,ae1s_beta;
    doublereal ae1s_nuVL,ae1s_AVL,ae1s_TVL;
    doublereal ae1s_DrVL1,ae1s_DphiVL1,ae1s_phiVL1;
    doublereal ae1s_DrVL2,ae1s_DphiVL2,ae1s_phiVL2;
    // template <size_t row, size_t colunm>
    doublereal Positive(doublereal X );
    void funcion_xi(doublereal* xi, doublereal phi, const doublereal* n0, doublereal* Keq);
    doublereal funcEq(const doublereal* NU, doublereal P,  doublereal T);
    doublereal H_RTTEq(const doublereal* NU, doublereal P,  doublereal T);
    void setState_HPX(doublereal H0, doublereal P, const doublereal* Y);

};

}

// #endif
